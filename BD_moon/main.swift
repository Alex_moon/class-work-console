import Foundation

var day = 0

var c = 0
var month = 0
for arg in CommandLine.arguments {
    if c == 1 {
        
        day = Int(arg) ?? 0
        if day > 31 {
        day = 0
        }
    }
    
    if c == 2 {
        
        month = Int(arg) ?? 0
        if month > 12 {
            month = 0
        }
    }
    print("argument \(c) is: \(arg)")
    c += 1
}

//print("prec = \(count)")

func getSecondsToDate (_ day : Int = 30, _ month : Int = 11) -> (day:Int, month: Int, seconds: Int) {
    let myDate = (day: day, month: month)
    let jan = (day: 31, month: 1)
    let feb = (day: 28, month: 2)
    let mar = (day: 31, month: 3)
    let apr = (day: 30, month: 4)
    let may = (day: 31, month: 5)
    let jun = (day: 30, month: 6)
    let jul = (day: 31, month: 7)
    let aug = (day: 31, month: 8)
    let sep = (day: 30, month: 9)
    let okt = (day: 31, month: 10)
    let nov = (day: 30, month: 11)
    var result = 0
    let secPerDay = 86400
    if myDate.month == jan.month {
        result = day
    }
    if myDate.month == feb.month {
        result = jan.day + day
    }
    if myDate.month == mar.month {
        result = jan.day + feb.day + day
    }
    if myDate.month == apr.month {
        result = jan.day + feb.day + mar.day + day
    }
    if myDate.month == may.month {
        result = jan.day + feb.day + mar.day + apr.day + day
    }
    if myDate.month == jun.month {
        result = jan.day + feb.day + mar.day + apr.day + may.day + day
    }
    if myDate.month == jul.month {
        result = jan.day + feb.day + mar.day + apr.day + may.day + jun.day + day
    }
    if myDate.month == aug.month {
        result = jan.day + feb.day + mar.day + apr.day + may.day + jun.day + jul.day + day
    }
    if myDate.month == sep.month {
        result = jan.day + feb.day + mar.day + apr.day + may.day + jun.day + jul.day + aug.day + day
    }
    if myDate.month == okt.month {
        result = jan.day + feb.day + mar.day + apr.day + may.day + jun.day + jul.day + aug.day + sep.day + day
    }
    if myDate.month == nov.month {
        result = jan.day + feb.day + mar.day + apr.day + may.day + jun.day + jul.day + aug.day + sep.day + okt.day + day
    }
    if myDate.month == 12 {
        result = jan.day + feb.day + mar.day + apr.day + may.day + jun.day + jul.day + aug.day + sep.day + okt.day + nov.day + day
    }
    result = (result - 1) * secPerDay
    return (day: day, month: month, seconds: result)
}

let dayOfBirthday  = day
let monthOfBirthday = month

print ("Seconds to date: \(getSecondsToDate(dayOfBirthday, monthOfBirthday).day).\(getSecondsToDate(dayOfBirthday, monthOfBirthday).month).2016 is \(getSecondsToDate(dayOfBirthday, monthOfBirthday).seconds) ")
